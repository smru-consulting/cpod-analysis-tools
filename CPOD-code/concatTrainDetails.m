%takes click train structures imported into Matlab using cpodFullTrain2mat
%and combines them in a single data structure
%Jason Wood March 14, 2012

function concatTrainDetails(file1, t1, file2, t2, file3, t3, file4, t4, file_out)

names = fieldnames(file1);  %get the structure field names

datatemp = [file1 file2 file3 file4];   %create an uber structure with all the data

%calc size of the different arrays
Nsiz(1,1) = size(datatemp(1,1).train_number,1);
Nsiz(2,1) = size(datatemp(1,2).train_number,1);
Nsiz(3,1) = size(datatemp(1,3).train_number,1);
Nsiz(4,1) = size(datatemp(1,4).train_number,1);

num_points = sum(Nsiz);

iStart = [1; Nsiz(1,1)+1; Nsiz(1,1)+Nsiz(2,1)+1; Nsiz(1,1)+Nsiz(2,1)+Nsiz(3,1)+1];
iStop = [Nsiz(1,1); Nsiz(1,1)+Nsiz(2,1); Nsiz(1,1)+Nsiz(2,1)+Nsiz(3,1); num_points];


%create the final structure size

data.train_number = zeros(num_points,1);
t = zeros(num_points,1);

%move the data over to the new structure array.

for i = 1:4
    data.train_number(iStart(i):iStop(i),1) = datatemp(1,i).train_number;
    data.species(iStart(i):iStop(i),1) = datatemp(1,i).species;
    data.quality(iStart(i):iStop(i),1) = datatemp(1,i).quality;
    data.train_duration(iStart(i):iStop(i),1) = datatemp(1,i).train_duration;
    data.num_clicks(iStart(i):iStop(i),1) = datatemp(1,i).num_clicks;
    data.actual_clicks(iStart(i):iStop(i),1) = datatemp(1,i).actual_clicks;
    data.click_rate(iStart(i):iStop(i),1) = datatemp(1,i).click_rate;
    data.mod_freq(iStart(i):iStop(i),1) = datatemp(1,i).mod_freq;
    data.avg_spl(iStart(i):iStop(i),1) = datatemp(1,i).avg_spl;
    data.ICI(iStart(i):iStop(i),:) = datatemp(1,i).ICI;
    data.lost(iStart(i):iStop(i),1) = datatemp(1,i).lost;
    data.minute_clicks(iStart(i):iStop(i),1) = datatemp(1,i).minute_clicks;
    data.last_ICI(iStart(i):iStop(i),1) = datatemp(1,i).last_ICI;
    data.ICI_rising(iStart(i):iStop(i),1) = datatemp(1,i).ICI_rising;
    data.more_freq(iStart(i):iStop(i),:) = datatemp(1,i).more_freq;
    data.Ncyc(iStart(i):iStop(i),:) = datatemp(1,i).Ncyc;
end

t(iStart(1):iStop(1),1) = t1;
t(iStart(2):iStop(2),1) = t2;
t(iStart(3):iStop(3),1) = t3;
t(iStart(4):iStop(4),1) = t4;

save([file_out], 'data', 't');
