function cpodclick2mat(file_path, file_in, file_out, PST_adjust, time_start, time_end,config)

%Reads C-pod click details into matlab
%Taken from cpod2mat function written by Brian Polagye May 23, 2011
%Tweaked by Jason Wood Sept 19, 2011

%% Setup

headerlines = 4;

%% Load CPod output file

disp(['Loading CPod file...'])
temp = importdata([file_path file_in '.txt'],'\t',headerlines);

% data structure
% 1: Microseconds
% 2: Cycles
% 3: SPL_Pa
% 4: kHz
% 5: Bandwidth
% 6: End kHz

% textdata structure
% 1,1 file name
% 1,2 click filters
% 1,3 train filters
% 1,4 time DD/MM/YYYY HH:MM (days and months can be two digits)


%allocate memory
num_points = size(temp.data,1);

t = zeros(num_points,1);
%data.train_number = zeros(num_points,1);
%data.species = zeros(num_points,1);
%data.quality = zeros(num_points,1);

%convert output
disp(['Converting ' file_in ' to .mat format...'])

%text data - account for header row
for j = 1:length(t)    
    if length(temp.textdata{j+4,1})<13
        t(j) = datenum(temp.textdata{j+4,1},'dd/mm/yyyy');
    else
        t(j) = datenum(temp.textdata{j+4,1},'dd/mm/yyyy HH:MM');
    end
    %data.train_number(j) = str2double(temp.textdata(j+1,2));
end
    

data.click_time = temp.data(:,1);
data.clicks_cycles = temp.data(:,2);
data.click_amp = temp.data(:,3);
data.click_freq = temp.data(:,4);
data.click_bandwidth = temp.data(:,5);
data.click_endfreq = temp.data(:,6);

%correct time stamps to PST
t = t + PST_adjust/24;

%truncate entries outside of start and end time
if time_start == 0; time_start = min(t); end;
if time_end == 0; time_end = max(t); end;

ind_keep = find(t>=time_start & t <=time_end);

t = t(ind_keep);

data_names = fieldnames(data);
for i = 1:length(data_names)
    data.(data_names{i}) = data.(data_names{i})(ind_keep,:);
end

%cleanup
clear temp

%% Save output in .mat format
save([file_path file_out],'t','data')
