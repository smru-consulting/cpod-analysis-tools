function [tidaldata] = tidalmergeTable(file_path)
%takes the BOF tidal data from Brian Sanderson caculated for each site and merges it into a single
%matrix
%Jason Wood May 2013

D = dir([file_path '**\*.mat']);  %list all the tidal mat files
k = 1;

tidaldata = table();

for ii = 1:size(D,1)
    
    %Load the data
    load(fullfile(D(ii).folder, D(ii).name));
    
    %create matdate vector and add 9 minutes to match CPOD data.
    tt = t1+9/(24*60):10/(24*60):t2+9/(24*60);   % Time
    speed = S'; %enter speed in m/s
    height = h'; %enter tidal height (zero = mean tidal heigth)
    
    
    % Location abbreviation 
    locName =D(ii).name(1,1:2);
    
      switch locName
        case 'D1'
            tempLoc = repmat({'D1'}, [length(speed),1]);
        case 'W2'
            tempLoc = repmat({'W2'}, [length(speed),1]);
        case 'W1'
            tempLoc = repmat({'W1'}, [length(speed),1]);
        case 'E1'
           tempLoc = repmat({'E1'}, [length(speed),1]);
        case 'S2'
           tempLoc = repmat({'S2'}, [length(speed),1]);
            
      end
    
    tempTable = table(tt', speed, height, tempLoc,...
        'VariableNames', {'MDate', 'TidalSpeed', 'TidalHeight', 'Loc'});
    
   tidaldata = [tidaldata;tempTable ];
end

 %round off the matdate to nearest minute to match with CPOD data
tidaldata.MDate = datenum_round_off(tidaldata.MDate ,'minute', 'floor');   