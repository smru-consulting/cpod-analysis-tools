function dpmBCI(file_path, file_in, file_out)
%takes the dpm data from CPODs that has been read in using the cpoddpm2mat
%function. It then calculates the latency between dpm to plot the Bout
%Criterion Interval to determine likely encounter duration (or rather the
%latency between encouters).
%Jason Wood 2012

%% Load mat file
disp('Loading .mat file...')

data = load([file_path file_in '.mat']);

%% Calculate latency
disp('Calculating latency ...')

dpmlatency = zeros(sum(data.dpm),1);    %since dpm are 0 or 1, the sum will be the total number of dpm
k = 1;  %latency index
j = 0;  %latency counter
for i = 1:size(data.dpm,1)
    if data.dpm(i,1) == 0
        j = j+1;
        %if there is a dpm, then record the data and rest counter
    else
        dpmlatency(k,1) = j;
        j = 0;
        k = k + 1;
    end
end

dpmlatency = dpmlatency(2:size(dpmlatency,1)-1);    %get rid of the first and last latencies since those are due to CPOD being turned on or off.

%% Plot the results to calculate traditional BCI

latencysort = sort(dpmlatency);     %sort the latency from small to large
cumfreq = 1:size(dpmlatency,1);
cumfreq = sort(cumfreq,'descend');

plot(latencysort,log(cumfreq));
title('Traditional BCI Plot');
ylabel('log(Cummulative Frequency)');
xlabel('Time Interval (min)');

%% Plot the BCI using Tolkamp and Kyriazakis 1999 method

%loglatency = log(dpmlatency);
%[latencyfreq, bin] = hist(loglatency,1000);
%probabilitydensity = latencyfreq/sum(latencyfreq);
%plot(bin,probabilitydensity);

loglatency = log(latencysort);
mu = mean(dpmlatency);
sd = std(dpmlatency);

for i = 1:size(dpmlatency,1)
    pdf(i,1) = (1/(sd*sqrt(2*pi)))*exp(-((loglatency(i,1)-mu).^2)/2*sd.^2);
end

