% Concatenates text output from CP3 files into a single text file for each
% deployment. CP3 output files must be organized by deployment e.g.
% /Force/depLoc1,  /Force/depLoc2, /Force/depLoc3, etc... 

% This skips the step of concatenating the cp3 files in CPOD.exe because
% they have a bunch of monkeys writing that software and it constantly
% buggs out. It also skips the cpodDetEnv2mat while maintaining the format

% Differences is this works with tables, rather than structures

close all; clear all; clc;

cd('C:\BitBucketRepositories\cpod-analysis-tools')

% Turn off the irritating warnings
warning('OFF', 'MATLAB:table:ModifiedAndSavedVarnames')

% Location of the project file containing the exported CP3 csvs
projectLoc = 'C:\Data\FORCE Data\2019 Aug - 2019 Dec\';
projectLoc = 'C:\Data\FORCE Data\2020EEMP\CP3Files\'
projectLoc ='C:\Data\FORCE Data\2020EEMP\10MINDetFiles\'
% Get the files within the project 
cp3CSVfiles = ((dir([projectLoc '**\*.txt'])));

%% Load the Tidal Data and merge it

tidalLoc = 'C:\Users\Kaitlin Palmer\Box\SMRUC NA\Client Projects\SMRU Canada\Active\FORCE\year 4 EEMP\Deliverables\Tidal covariates\';
tidalLoc ='C:\Data\FORCE Data\2020EEMP\TidalCovariates\'
[tidaldata] = tidalmergeTable(tidalLoc);


% Create datetime for interpretation
TidalData.dateTime = datetime(tidaldata.MDate,'ConvertFrom','datenum');

%%


% loop throught the files, concatenate the structures
T = table();
currentFolder = cp3CSVfiles(1).folder;


% Trim any data beyond the last monitored day (one deployment appears the
% pod kept running two days after recovery

maxDate = datenum('10/14/2021', 'mm/dd/yyyy');
minDate = datenum('06/01/2020 23:00:00', 'mm/dd/yyyy HH:MM:SS');
minDate = datetime(minDate,'ConvertFrom','datenum')


% add soak time of 12 hours after each deployment and 1hour pre deployment
soakTime = hours(12);
recoveryTime = hours(1);



for ii=1:length(cp3CSVfiles)
    
    % Check if the current folder matches the previous one. If not, export
    % the concatenated version and start a new table
     
     
     opts = detectImportOptions(fullfile(cp3CSVfiles(ii).folder, cp3CSVfiles(ii).name));
     opts =setvaropts(opts, 'ChunkEnd','InputFormat','dd/MM/uuuu HH:mm');

     %get the header
     fid = fopen(fullfile(cp3CSVfiles(ii).folder, cp3CSVfiles(ii).name));    %fid = fopen([file_path file_in '.txt']);
     headerlines = opts.DataLines(1)-1
     header = textscan(fid, '%s',headerlines, 'delimiter', '\n');    %read header data
     fclose(fid);
     
     % CPOD number
     pod =  strsplit(header{1,1}{2}, ' ')
    
     
    % load the file
    try
        
    newData = readtable(fullfile(cp3CSVfiles(ii).folder, cp3CSVfiles(ii).name), opts);
    catch err
        
        disp('Weird Data, trying to load another way')
        
        % get the read table options (we will fix later)
        
        
        opts.DataLines = opts.DataLines+1;
        try  newData = ...
                readtable(fullfile(cp3CSVfiles(ii).folder, cp3CSVfiles(ii).name), opts);
        catch
             opts.DataLines = opts.DataLines+1;
             
             try newData = ...
                readtable(fullfile(cp3CSVfiles(ii).folder, cp3CSVfiles(ii).name), opts);
             catch
                 disp('Revisit Code')
             end
        end
    end
          
        
    % Check for NAN's in first few lines, delete 
    newData( any(ismissing(newData),2), :) = [];
    
    % Add POD ID from the header
    newData.pod = ones(height(newData), 1) * str2num( pod{2});
    
    matlabDates = datenum(newData.ChunkEnd);
    if any(matlabDates> maxDate)
        disp('blarg')
        
        newData = newData(matlabDates<=maxDate,:);
    end
    
    
    % Create Variable location from the deployment location
    %locations: N1=1; W2=2; W1=3; E1=4; E2=5; S1=6; S2=7; D1 = 8
    
    
    loc = cp3CSVfiles(ii).name(1:2);
    newData.LocName = repmat(loc, [height(newData), 1]);
    newData.area = zeros(height(newData), 1);
    switch loc
        case 'N1'
            newData.location =  ones(height(newData), 1) *1;
            
        case 'W2'
            newData.location =  ones(height(newData), 1) *2;
            
            % Trim based on recovery date and deployment date plus 12 hours
            newData = newData(newData.ChunkEnd<=...
             datetime('2020-11-08 12:00:00',...
            'InputFormat','yyyy-MM-dd HH:mm:ss')-recoveryTime,:);
        
            newData = newData(newData.ChunkEnd>=...
             datetime('2020-06-12 14:07:00',...
            'InputFormat','yyyy-MM-dd HH:mm:ss')+soakTime,:);        
            
            
        case 'W1'
            newData.location =  ones(height(newData), 1) *3;
            newData.area = ones(height(newData), 1);
            
            
            % Trim based on recovery date
            newData = newData(newData.ChunkEnd<=...
                datetime('2020-11-06 13:47:15',...
                'InputFormat','yyyy-MM-dd HH:mm:ss')-recoveryTime,:);
            
            newData = newData(newData.ChunkEnd>=...
                datetime('2020-06-12 14:15:00',...
                'InputFormat','yyyy-MM-dd HH:mm:ss')+soakTime,:);
            
         
%             datetime('2020-01-14 17:32:00',...
%             'InputFormat','yyyy-MM-dd HH:mm:ss'),:);...        
        case 'E1'
            newData.location =  ones(height(newData), 1) *4;
            newData.area = ones(height(newData), 1);
            
            
            % Trim based on recovery date
            newData = newData(newData.ChunkEnd<=...
                datetime('2020-11-06 14:05:10',...
                'InputFormat','yyyy-MM-dd HH:mm:ss')-recoveryTime,:);
            
            
            newData = newData(newData.ChunkEnd>=...
                datetime('2020-06-12 14:27:00',...
                'InputFormat','yyyy-MM-dd HH:mm:ss')+soakTime,:);
            
            %             datetime('2019-12-13 15:43:00',...
%             'InputFormat','yyyy-MM-dd HH:mm:ss'),:);     
        case 'E2'
            newData.location =  ones(height(newData), 1) *5;
            
            
        case 'S1'
            newData.location =  ones(height(newData), 1) *6;
            
            
        case 'S2'
            newData.location =  ones(height(newData), 1) *7;
            
            
            % Trim based on recovery date
            newData = newData(newData.ChunkEnd<=...
                datetime('2020-11-06 13:12:21',...
                'InputFormat','yyyy-MM-dd HH:mm:ss')-recoveryTime,:);
            
            newData = newData(newData.ChunkEnd>=...
                datetime('2020-06-12 13:57:00',...
                'InputFormat','yyyy-MM-dd HH:mm:ss')+soakTime,:);
            
            
            %             datetime('2019-12-13 17:25:00',...
            %             'InputFormat','yyyy-MM-dd HH:mm:ss'),:);

        case 'D1'
            newData.location =  ones(height(newData), 1) *8;
            newData.area = ones(height(newData), 1);
            
            % Trim based on recovery date
            newData = newData(newData.ChunkEnd<=...
                datetime('2020-09-23 07:45:00',...
                'InputFormat','yyyy-MM-dd HH:mm:ss'),:);
            
            
            newData = newData(newData.ChunkEnd>=...
                datetime('2020-06-12 14:24:00',...
                'InputFormat','yyyy-MM-dd HH:mm:ss')+soakTime,:);
            
            
        
  %             datetime('2019-12-13 16:32:00',...
%             'InputFormat','yyyy-MM-dd HH:mm:ss'),:);      
        otherwise
            disp('BLOODY HELL!')
    end
    
  

    
    
    % check first line of the new data with last line of the previous data
    if ~isempty(T) && strcmp(currentFolder, cp3CSVfiles(ii).folder)
        % Some CPOD export files as cells (mixed numbers and letter) need to
        % address those..
        
        if iscell(T.Nfiltered_m)
            T.Nfiltered_m =cellfun(@str2num, T.Nfiltered_m);
        end
        if iscell(T.Nall_m)
            T.Nall_m =cellfun(@str2num, T.Nall_m);
        end
        if iscell(T.SonarRisk)
            T.SonarRisk =cellfun(@str2num, T.SonarRisk);
        end
        
        if iscell(T.x_TimeLost)
            T.x_TimeLost =cellfun(@str2num, T.x_TimeLost);
        end
        
        if iscell(T.kHz_continuous_noise)
            T.kHz_continuous_noise =cellfun(@str2num, T.kHz_continuous_noise);
        end
        
        if iscell(T.LandmarkSeq_total)
            T.LandmarkSeq_total =cellfun(@str2num, T.LandmarkSeq_total);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % This one is on me
        if  ~strcmp('Mdate',newData.Properties.VariableNames)
            newData.Mdate = datenum(newData.ChunkEnd);
        end
        
        if  ~strcmp('Mdate',T.Properties.VariableNames)
            T.Mdate = datenum(T.ChunkEnd);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Extract the last line of T
        t_last_orig = T(end,:);
        
        aa = newData(1,:);
        fixed_ros = [t_last_orig; aa];
        
        % new output to replace the last row in the incomplete file
        outCol = t_last_orig;
        colName = aa.Properties.VariableNames;
        
        for jj = 1:length(colName)
            
            switch colName{jj}
                
                % for cases where we want the average
                case {'Temp' , 'Angle', 'x_TimeLost', 'SonarRisk',...
                        'kHz_continuous_noise'}
                    outCol.(colName{jj}) = mean(fixed_ros.(colName{jj}));
                    
                    % for cases wehre we want the sum
                case {'MinutesON', 'DPM', 'Nfiltered_m', 'Nall_m',...
                        'LandmarkSeq_total'}
                    try
                    outCol.(colName{jj}) = sum(fixed_ros.(colName{jj}));
                    catch
                        disp('feck')
                        
                        
                        
                    end
                    
                    % For cases where we want the value from one column
                case {'File', 'opts', 'JulianDay', 'ChunkEnd',...
                        'pod', 'LocName','area', 'location'}
                    
                    outCol.(colName{jj}) = aa.(colName{jj});
                    
                otherwise
                    disp(['Error on ' colName{jj}])
                    break
            end
            
            
        end
        
        
        % Replace the last row of the old table with the fixed row and nix
        % the first row of the new table
        
        T = T(1:end-1,:);
        T = [T; outCol; newData(2:end,:)];
        
        % Convert datetime to datenum for interpolation
        T.Mdate = datenum(T.ChunkEnd);
        
        disp(['File ' ,   cp3CSVfiles(ii).name, ' appended'])
       
    else
        
        disp(['New Folder ' cp3CSVfiles(ii).folder])
        
        % If T is already populated trim the data and write it out
        if ~isempty(T)
            
            
            % Convert datetime to datenum for interpolation
            T.Mdate = datenum(T.ChunkEnd);
            % Trim the table
            T = T(T.ChunkEnd> minDate,:);
            T = sortrows(T,'ChunkEnd','ascend');
            
            % Match the tidal Data 
            % subset
            %tidalSub = tidaldata((strcmp(TidalData.Loc, T.LocName(1,:))),:);
            
            tidalSub = tidaldata((strcmp(tidaldata.Loc, T.LocName(1,:))),:);
            [~, index] = unique(tidalSub.MDate); 
            tidalSub= tidalSub(index,:);
            % Sort it
            tidalSub = sortrows(tidalSub,'MDate','ascend');
            
            % Interpolate time
            aa = interp1(tidalSub.MDate, tidalSub.TidalHeight, T.Mdate,...
                'nearest');
            
            
            save([currentFolder,'\'...
                T.LocName(1,1:2), '_Concat'],'T', 'header');
            disp('File Written')
        
        end
            
        
        T = newData;
        currentFolder = cp3CSVfiles(ii).folder;
        
    end
    
    
    % Kludge for last iteration
    if  ii == length(cp3CSVfiles)
            
            % Trim the table
            T = T(T.ChunkEnd> minDate,:);
           
        
            
            tidalSub = tidaldata((strcmp(tidaldata.Loc, T.LocName(1))),:);
            
            save([currentFolder,'\'...
                T.LocName(1,1:2), '_Concat'],'T', 'header');
            disp('File Written')
    end
    
    clear newData


end





