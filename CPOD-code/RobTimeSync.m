%takes the raw CPOD data and compares the times with the times and dates
%Rob used for his analyses. It then only keeps click train detail data from
%those times
%Jason Wood March 14, 2012

function RobTimeSync(file_path, file_in, sheet_in, file_out, pod_file)
%%load data
disp(['Loading CPod file...'])
load([file_path pod_file '.mat']);    %load the .mat file with all the CPOD data

%read in Rob's spreadsheet with time/date format added in worksheet
%'working'
disp(['Loading Excel file...'])
 [num, txt] = xlsread([file_path file_in '.xlsx'], [sheet_in]);

%%work on the data

 disp(['Selecting click train data from ' pod_file '.mat...'])
 %convert times to matlab times
 Robtime = num(:,15) + 693960;  %693960 is the difference between excel and matlab date/time number format
  
 %compare times
 
 addhour = 1/24;    %1 hour in matlab datenum is 1/24th of a day
 k = 1;
 tind = zeros(length(t),1);
 for i = 1:length(Robtime)
     for j = 1:length(t)
         if t(j,1) >= Robtime(i) && t(j,1) < Robtime(i)+addhour    %find times during Rob's hours
             tind(j,1) = 1;  %keep track of the ones that were analyzed
             hourind(k,1) = i;   %keep an hour index that relates back to Rob's data
             k = k+1;
         end
     end
 end
 
%truncate data so only have data matching  

ind_keep = find(tind >0);

t = t(ind_keep);

data_names = fieldnames(data);
for i = 1:length(data_names)
    data.(data_names{i}) = data.(data_names{i})(ind_keep,:);
end

%% Save output in .mat format
save([file_path file_out],'t','data', 'txt', 'num', 'hourind');