function cumprobplot(x, data1, data2, data3, data4)
% x = 1:100;

[n1,x] = hist(data1,x);
prob1 = n1/size(data1,1);
plot(x,cumsum(prob1), 'Linewidth', 2);

hold on;

[n2,x] = hist(data2,x);
prob2 = n2/size(data2,1);
plot(x,cumsum(prob2),'r', 'Linewidth', 2);

[n3,x] = hist(data3,x);
prob3 = n3/size(data3,1);
plot(x,cumsum(prob3),'g', 'Linewidth', 2);

[n4,x] = hist(data4,x);
prob4 = n4/size(data4,1);
plot(x,cumsum(prob4),'k', 'Linewidth', 2);

%[n5,x] = hist(data5,x);
%prob5 = n5/size(data5,1);
%plot(x,cumsum(prob5),'--b', 'Linewidth', 2);

set(gca,'Fontsize', 12, 'Fontweight', 'b');
ylabel('Cummulative Probability', 'Fontsize',12, 'Fontweight', 'b');
xlabel('Percent Time Lost', 'Fontsize',12, 'Fontweight', 'b');
ylim([0 1]);

legend('2790-D1','2765-E1', '2793-W1', '2792-W2','Location','SouthEast');
