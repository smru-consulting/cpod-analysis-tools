%modified from Brian Polagye's cpod2mat function
%Jason Wood
%March 14, 2012

%Description: convert CPod full train detail file to .mat structure

function cpodFullTrain2mat(file_path, file_in, file_out, PST_adjust, time_start, time_end)

%% Setup

headerlines = 1;

%% Load CPod output file

disp(['Loading CPod file...'])
temp = importdata([file_path file_in '.txt'],'\t',headerlines);

% data structure
% 1: start - start time within minute in mircosec
% 2: train duration (us)
% 3: number of clicks in train
% 4: number of actual clicks in train (not including 'virtual clicks')
% 5: number of clicks/s (rounded to whole number)
% 6: frequency (kHz, mode)
% 7: average SPL (units?)
% 8: maximum interclick interval (ICI) (us)
% 9: minimum interclick interval (ICI) (us)
% 10: MMM: Mean multipath minimum � mean of the SPL of the weakest clicks in the multipath clusters identified
% 11: % time lost due to filling click buffer
% 12: clicks this minute (total in CP1 file)
% 13: last ICI (us)
% 14: ICI rising (%ICI longer than previous ICI, high when WUTS)
% 15: avg end freq of click
% 16: min freq
% 17: max freq
% 18: avNcyc - avg number of cycles in all clicks in train
% 19: MaxNcyc - max number of cycles in all clickcs in train
% 20: MaxSPL - highest raw SPL of any click in train
% 21: NofClstrs - number of clicks that are part of a mulitipath cluster
% 22: avClstrN*10 - avg number of multipath replicates in clusters * 10
% 23: Frange - max - min click hHz
% 24: avSlope1 - these last 3 variables are iffy given analysis window
% 25: avSlope2
% 26: avBW

% textdata structure
% 1: file name
% 2: train number
% 3: time DD/MM/YYYY HH:MM (days and months can be two digits)
% 4: internal CPod time
% 5: species class: "NBHF" = harbor porpoise
% 6: train class: "Mod","High"

%allocate memory
num_points = size(temp.data,1);

t = zeros(num_points,1);
data.train_number = zeros(num_points,1);
data.species = zeros(num_points,1);
data.quality = zeros(num_points,1);

%convert output
disp(['Converting ' file_in ' to .mat format...'])

%text data - account for header row
for j = 1:length(t)    
    if length(temp.textdata{j+1,3})<13
        t(j) = datenum(temp.textdata{j+1,3},'dd/mm/yyyy');
    else
        t(j) = datenum(temp.textdata{j+1,3},'dd/mm/yyyy HH:MM');
    end
    data.train_number(j) = str2double(temp.textdata(j+1,2));
    
    %species classification
    if strcmp(temp.textdata(j+1,5),'NBHF')
        data.species(j) = 1;    %porpoise
    elseif strcmp(temp.textdata(j+1,5),'NoSp')
        data.species(j) = 2;    %unknown
    elseif strcmp(temp.textdata(j+1,5),'Sonar')
        data.species(j) = 3;    %sonar
    else
        data.species(j) = 4;    %dolphin
    end
    
    %train classification quality
    if strcmp(temp.textdata(j+1,6),'High')
        data.quality(j) = 1;    %high quality trains
    elseif strcmp(temp.textdata(j+1,6),'Mod')
        data.quality(j) = 2;    %moderate quality trains
    elseif strcmp(temp.textdata(j+1,6),'Low')
        data.quality(j) = 3;    %low quality trains
    else
        data.quality(j) = 4;    %doubtful trains
    end
end

%enter data that am interested in to the structure array
data.train_duration = temp.data(:,2);
data.num_clicks = temp.data(:,3);
data.actual_clicks = temp.data(:,4);
data.click_rate = temp.data(:,5);
data.mod_freq = temp.data(:,6);
data.avg_spl = temp.data(:,7);
data.ICI = temp.data(:,[8,9]);
data.lost = temp.data(:,11);
data.minute_clicks = temp.data(:,12);
data.last_ICI = temp.data(:,13);
data.ICI_rising = temp.data(:, 14);
data.more_freq = temp.data(:,15:17);    %avg end, min, then max
data.Ncyc = temp.data(:,18:19); %avg then Max N cycles

%correct time stamps to PST
t = t + PST_adjust/24;  %t is the date / time and is stored as a datenum

%truncate entries outside of start and end time
if time_start == 0; time_start = min(t); end;
if time_end == 0; time_end = max(t); end;

ind_keep = find(t>=time_start & t <=time_end);

t = t(ind_keep);

data_names = fieldnames(data);
for i = 1:length(data_names)
    data.(data_names{i}) = data.(data_names{i})(ind_keep,:);
end

%cleanup
clear temp

%% Save output in .mat format
save([file_path file_out],'t','data')