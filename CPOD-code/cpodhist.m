function cpodhist(data1, data2, data3, datatype, dataclass, pod1, pod2, pod3)

%function to plot histograms of different click variables to compare 3 pods

%inputs:
%data1, 2 and 3, are the structures with the click data for each pod
%datatype is the click variable you want plotted as follows:
% 1 = click time
% 2 = cycles
% 3 = amplitude (SPL Pa)
% 4 = Frequency (kHz)
% 5 = Bandwidth
% 6 = end frequency (kHz)
%dataclass is the label that will be added to denote the kind of click (eg
%sonar, hammer, etc)

%populate datavar to use as xlabel in graphs
datavar = cell(6,1);
datavar{1,1} = 'Click time';
datavar{2,1} = 'Cycles';
datavar{3,1} = 'SPL';
datavar{4,1} = 'Frequency (kHz)';
datavar{5,1} = 'Bandwidth';
datavar{6,1} = 'End Frequency (kHz)';

%extract field names from structure array
names = fieldnames(data1);
filename = who('data*');

%generate string sections for plotting
A = '[N,X] = hist(';
B = '.';
C = ');';


subplot(1,3,1)
eval([A filename{1} B names{datatype} C]);
bar(X,N);
title([pod1 names(datatype) dataclass]);
ylabel('Count');
Xmax(1,1) = max(eval([filename{1} B names{datatype}]));
Ymax(1,1) = max(N);

subplot(1,3,2)
eval([A filename{2} B names{datatype} C]);
bar(X,N);
title([pod2 names(datatype) dataclass]);
xlabel(datavar{datatype,1});
Xmax(2,1) = max(eval([filename{2} B names{datatype}]));
Ymax(2,1) = max(N);

subplot(1,3,3)
%D = sprintf(datanames{3});
eval([A filename{3} B names{datatype} C]);
bar(X,N);
title([pod3 names(datatype) dataclass]);
Xmax(3,1) = max(eval([filename{3} B names{datatype}]));
Ymax(3,1) = max(N);

%set standard axes
Xlo = 0;
Xhi = max(Xmax);
Ylo = 0;
Yhi = max(Ymax)+50;

subplot(1,3,1)
xlim([Xlo Xhi]);
ylim([Ylo Yhi]);

subplot(1,3,2)
xlim([Xlo Xhi]);
ylim([Ylo Yhi]);

subplot(1,3,3)
xlim([Xlo Xhi]);
ylim([Ylo Yhi]);
