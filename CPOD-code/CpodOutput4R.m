%takes the CPOD click train data for analysis (from RobTimeSync) and the
%data from Rob's excel file and outputs it into a .csv file for analysis in
%R.
%Jason Wood March 19, 2012

function CpodOutput4R(file_path, file_in, file_out)

%%Header info for file_out

Header = cell(1,1+20+11+1);   %1 time index, 20 data, 11 num, 1 date_time
Header{1,1} = 'Time_index';
data_names = fieldnames(data)';
for i = 1:9
    Header{1,i+1} = data_names{1,i};
end
Header{1,11} = 'ICI_max';
Header{1,12} = 'ICI_min';
for i = 1:4
    Header{1,i+12} = data_names{1,i+10};
end
Header{1,17} = 'avg_end_freq';
Header{1,18} = 'min_freq';
Header{1,19} = 'max_freq';
Header{1,20} = 'avg_Ncyc';
Header{1,21} = 'max_Ncyc';
for i = 1:11
    Header{1,i+21} = txt{1,i+1};
end
Header{1,33} = 'Date_time';

%%Create output matrix

data_out = zeros(size(t,1),size(Header,2));

data_out(:,1) = hourind;
for i = 1:9
    data_out(:,i+1) = data.(data_names{i});
end
data_out(:,11:12) = data.ICI;
for i = 1:4
    data_out(:,i+12) = data.(data_names{i+10});
end
data_out(:,17:19) = data.more_freq;
data_out(:,20:21) = data.Ncyc;

for i = 1:size(data_out,1)
    data_out(i,22:32) = num(data_out(i,1),2:12);
end
data_out(:,33) = t;     %this is Matlab time format!

clear data t num txt datanames hourind datanames;   %make more memory room

csvwrite_with_headers('718_data.csv',data_out,Header);  %write the file to csv

