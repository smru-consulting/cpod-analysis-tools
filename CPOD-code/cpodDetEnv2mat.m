function cpodDetEnv2mat(file_path,file_in, file_out, time_start, time_end)

%reads the Cpod detections and environment export text files into matlab
%based on cpod2mat.m by Brian Polagye
%Jason Wood 4/9/2012
%time_start and end must be in matlab date time format
%Can use the following code to generate that
%time_start = datenum('2/5/2011 14:34', 'mm/dd/yyyy HH:MM');
%or if want entire file use 0 for start and end
%file_path = 'T:\smru-ftp\Projects\FORCE CPOD monitoring\Jan 2017 retrieval\';
%file_in = 'Minas Passage 2016 09 21 POD2765 joined NB DetEnv.txt';
%file_out = 'POD2765Jan.mat';

%under current setup temperature gets rounded to nearest degree during import

%% Setup

headerlines = 9;    %number of header lines in text file
%this seems to change over time!!!
%for 1 minute bins use 7 for headerlines
%for 10 minute bins use 11 for headerlines
%for 1 hour bins use 11 for headerines!
colheaderlines = 1; %number of header lines for data columns
%% Load CPod output file

disp('Loading CPod file...')
fid = fopen([file_path file_in]);    %fid = fopen([file_path file_in '.txt']);
header = textscan(fid, '%s',headerlines, 'delimiter', '\n');    %read header data
colheader = textscan(fid, '%s %s %s %s %s %s %s %s %s %s %s %s %s', ...
    colheaderlines, 'delimiter', '\t');  %read column headings
temp = textscan(fid,'%s %s %d %d %d %d %d %d %d %d %d %d %d',...
    'delimiter', '\t', 'HeaderLines', 1);  %read the data (also imports LandmarkSeq column)
temp{1,13} = [];    %delete the LanmarkSeq
fclose(fid);

%add a Matlab date number
for i = 1:length(temp{1,2}(:,1))
    temp{1,13}(i,1) = datenum(temp{1,2}(i,1), 'dd/mm/yyyy HH:MM');
end

%add the header info to the cell structure
temp{1,14} = header;

%% Convert cell array to structure array without file name

fields = {'strdate', 'cpodmin', 'Temp', 'Angle', 'minon', 'dpm',...
    'Nfilt_min', 'Nall_min', 'TimeLost', 'sonarRisk', 'kHz_noise',...
    'matdate' 'header'};
data = cell2struct(temp(:,[2:14]),fields,2);

clear temp;

%truncate entries outside of start and end time
if time_start == 0; time_start = min(data.matdate); end;

if time_end == 0; time_end = max(data.matdate); end;

if size(data.matdate,1) > size(data.kHz_noise,1);
    time_end = max(data.matdate)-1/1440;
end;  %deal with the fact that CPOD output sometimes doesn't fill last row of data

ind_keep = find(data.matdate>=time_start & data.matdate <=time_end);

data_names = fieldnames(data);  %used for indexing
for i = 1:length(data_names)-1
    data.(data_names{i}) = data.(data_names{i})(ind_keep,:);
end
%% Save file to mat

save([file_path file_out],'data');

%also add start and end times so can synchronize across pods.

