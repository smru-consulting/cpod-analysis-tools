%remove first and last rows of data from C-POD data structures to get rid
%of 10 minute periods that have less than 10 minutes of data.

files = who;

for i = 1:size(files,1)
    A = [files{i,1} '.strdate = ' files{i,1} '.strdate(2:end-1,1);'];
    eval(A);
    A = [files{i,1} '.cpodmin = ' files{i,1} '.cpodmin(2:end-1,1);'];
    eval(A);
    A = [files{i,1} '.Temp = ' files{i,1} '.Temp(2:end-1,1);'];
    eval(A);
    A = [files{i,1} '.Angle = ' files{i,1} '.Angle(2:end-1,1);'];
    eval(A);
    A = [files{i,1} '.minon = ' files{i,1} '.minon(2:end-1,1);'];
    eval(A);
    A = [files{i,1} '.dpm = ' files{i,1} '.dpm(2:end-1,1);'];
    eval(A);
    A = [files{i,1} '.Nfilt_min = ' files{i,1} '.Nfilt_min(2:end-1,1);'];
    eval(A);
    A = [files{i,1} '.Nall_min = ' files{i,1} '.Nall_min(2:end-1,1);'];
    eval(A);
    A = [files{i,1} '.TimeLost = ' files{i,1} '.TimeLost(2:end-1,1);'];
    eval(A);
    A = [files{i,1} '.sonarRisk = ' files{i,1} '.sonarRisk(2:end-1,1);'];
    eval(A);
    A = [files{i,1} '.kHz_noise = ' files{i,1} '.kHz_noise(2:end-1,1);'];
    eval(A);
    A = [files{i,1} '.matdate = ' files{i,1} '.matdate(2:end-1,1);'];
    eval(A);
end
