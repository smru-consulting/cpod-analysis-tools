function cpoddpm2mat(file_path,file_in, file_out, time_start, time_end)

%reads the Cpod dpm export text files into matlab
%based on cpod2mat.m by Brian Polagye
%Jason Wood 3/1/2012
%time_start and end must be in matlab date time format
%Can use the following code to generate that
%time_start = datenum('2/5/2011 14:34', 'mm/dd/yyyy HH:MM');

%% Setup

headerlines = 7;    %number of header lines in text file
colheaderlines = 1; %number of header lines for data columns
%% Load CPod output file

disp('Loading CPod file...')
fid = fopen([file_path file_in '.txt']);
header = textscan(fid, '%s',headerlines, 'delimiter', '\t');    %read header data
colheader = textscan(fid, '%s %s %s %s %s %s', colheaderlines, 'delimiter', '\t');  %read column headings
temp = textscan(fid,'%s %s %d %d %d %d', 'delimiter', '\t', 'HeaderLines', 1);  %read the data
fclose(fid);

%add a Matlab date number
for i = 1:length(temp{1,2}(:,1))
    temp{1,7}(i,1) = datenum(temp{1,2}(i,1), 'dd/mm/yyyy HH:MM');
end

%add the header info to the cell structure
temp{1,8} = header;

%% Convert cell array to structure array without file name

fields = {'strdate', 'cpodmin', 'dpm', 'Nall', 'minon', 'matdate' 'header'};
data = cell2struct(temp(:,[2:8]),fields,2);

clear temp;

%truncate entries outside of start and end time
if time_start == 0; time_start = min(data.matdate); end;
if time_end == 0; time_end = max(data.matdate); end;

ind_keep = find(data.matdate>=time_start & data.matdate <=time_end);

data_names = fieldnames(data);  %used for indexing
for i = 1:length(data_names)-1
data.(data_names{i}) = data.(data_names{i})(ind_keep,:);
end
%% Save file to mat

save([file_path file_out],'data');

%also add start and end times so can synchronize across pods.

