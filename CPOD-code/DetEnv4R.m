function DetEnv4R()
%takes the data array files in the workspace, combines them and exports
%them as a CSV file for analysis in R
%Jason Wood 4/17/2012

%determine which files are in the workspace
files = who;

%determine which vectors are present in each array
A = 'data_names = fieldnames(';
B = files{1,1};
C = ');';
D = [A B C];
eval(D);
data_names = data_names';
clear A B C D;

%create output matrix of appropiate size
columns = 22;     %set how many output columns there will be

A = 'temp = size(';
C = '.dpm, 1);';
for i = 1: size(files,1)
    B = files{i,1};
    D = [A B C];
    eval(D);
    rows(i,1) = temp;
end
clear A B C D temp;

output = zeros(sum(rows(:,1)), columns);
    
%% now run through data and input it into output matrix

%set ouput order of columns

out_names{1,1} = data_names{1,12};  %matdate: will be used to cacl year
out_names{1,2} = data_names{1,12};  %matdate: will be used to cacl month, but placeholder
out_names{1,3} = data_names{1,12};  %matdate: will be used to cacl day, but placeholder
out_names{1,4} = data_names{1,12};  %matdate: will be used to cacl hour, but placeholder
out_names{1,5} = data_names{1,12};  %matdate: will be used to cacl minute, but placeholder
out_names{1,6} = data_names{1,12};  %matdate
out_names{1,7} = 'pod';  %pod
out_names{1,8} = 'location';  %locations: N1=1; W2=2; W1=3; E1=4; E2=5; S1=6; S2=7; D1 = 8
out_names{1,9} = 'area';  %area: inside FORCE area = 1; outside = 0
out_names{1,10} = data_names{1,6};   %dpm
out_names{1,11} = data_names{1,9};   %Time lost
out_names{1,12} = data_names{1,3};   %temperature
out_names{1,13} = data_names{1,4};  %Tilt angle
out_names{1,14} = data_names{1,8};  %all clicks
out_names{1,15} = 'clickmax'; %The max click set for memory buffer
out_names{1,16} = 'JulianDay'; %JD
out_names{1,17} = 'DayNightIndex'; %DNI (0 and 2 = dawn, 1 = dusk so day is 0 to 1 and night is 1 to 2)
out_names{1,18} = 'Panel'; %was originally 2 hours/panel (i.e. 12 10 minute periods)
out_names{1,19} = 'TimeLostClass'; %as per name
out_names{1,20} = 'TidalSpeed'; %+ is flood; - is ebb
out_names{1,21} = 'TidalHeight'; %0 is mean height
out_names{1,22} = 'BinDPM'; %binary DPM variable

%deal with time offset for UTC to local time
%during daylight savings UTC -3 = local time
%during non-daylight savings UTC - 4 = local time
timeoffset = -4;    %set the time offset in hours during STANDARD TIME.
for i = 1: size(files,1)
    outtemp = zeros(rows(i,1),columns);
    UTC = zeros(rows(i,1),1);
    %deal with date/time first
    A = 'datetemp = datevec(';
    B = [files{i,1} '.' out_names{1,1}];
    C = '+(timeoffset/24));';
    D = [A B C];
    eval(D);
    
    outtemp(:,1:5) = datetemp(:,1:5);
    A = 'outtemp(:,6) =';
    C = '+(timeoffset/24);';
    D = [A B C];
    eval(D);
    
    A = 'UTC(:,1) =';
    C = ';';
    D = [A B C];
    eval(D);    
    clear datetemp A B C D;
    
    UTC = datenum_round_off(UTC,'minute','round'); %make sure the datenum is rounded to the nearest minute.
    
    %%%%%THIS SECTION NEEDS CHANGING FOR EACH DOWNLOAD PERIOD!!!!
    %deal with switch of daylight savings
    %ondaylight = datenum(2017,3,12,2,09,0); %this is set to 2:09 due to Det&Env CPOD data being in 10 minute bins!
    %offdaylight = datenum(2017,11,5,2,09,0);  %ditto
    ondaylight = datenum(2019,3,10,6,09,0); %this is set to 2:09 due to Det&Env CPOD data being in 10 minute bins! SET THIS IN UTC SO SEARCH WORKS!
    offdaylight = datenum(2019,11,3,5,09,0);  %ditto
    index1 = find(UTC == ondaylight);
    index2 = find(UTC == offdaylight);
    %data are already converted to daylight savings time, so only need to
    %subtract 1 hour from times when are not on daylight savings in
    %Halifax!!!!
    
    %if data spans ondaylight (i.e. start after offdaylight, but is
    %offdaylight until ondaylight and does not reach the next offdaylight)
    if size(index1,1) == 1 & size(index2,1) == 0
        outtemp(index1:end,6) = outtemp(index1:end,6)+(1/24);
        [outtemp(index1:end,1),outtemp(index1:end,2),outtemp(index1:end,3),outtemp(index1:end,4),outtemp(index1:end,5)] = datevec(outtemp(index1:end,6));
    end
    
    %if data spans offdaylight (i.e. starts after ondaylight, but is
    %ondaylight until offdaylight and does not reach the next offdaylight)
    if size(index1,1) == 0 & size(index2,1) == 1
        outtemp(1:index2-1,6) = outtemp(1:index2-1,6)+(1/24);
        [outtemp(1:index2-1,1),outtemp(1:index2-1,2),outtemp(1:index2-1,3),outtemp(1:index2-1,4),outtemp(1:index2-1,5)] = datevec(outtemp(1:index2-1,6));
    end
    
    %if all data are during daylight savings
    if size(index1,1) == 0 & size(index2,1) == 0 & outtemp(end,6) < offdaylight
        outtemp(:,6) = outtemp(:,6)+(1/24);
        [outtemp(:,1),outtemp(:,2),outtemp(:,3),outtemp(:,4),outtemp(:,5)] = datevec(outtemp(:,6));
    end
    
     %if all data are during standard time
    if size(index1,1) == 0 & size(index2,1) == 0 & outtemp(1,6) > offdaylight
        %do nothing as 4 hours already subtracted
    end
        
    %on/offdaylight are ~6 months apart so shouldn't get times when data
    %span both so don't need more if statements...
    
    outtemp(:,6) = datenum_round_off(outtemp(:,6),'minute','round'); %make sure the datenum is rounded to the nearest minute.
    %outtemp(:,6) = datenum_round_off(outtemp(:,6),'minute','round'); %make sure the datenum is rounded to the nearest minute.
    
    %pod number THIS NEEDS TO BE CHANGED IF CPOD LOCATION CHANGES!!!!
    outtemp(:,7) = str2num(files{i,1}(5:8)); %stores the pod number
    %deal with location code and area
    if outtemp(1,7) == 2765
        outtemp(:,8) = 4;
        outtemp(:,9) = 1;
    elseif outtemp(1,7) == 2790
        outtemp(:,8) = 8;
        outtemp(:,9) = 1;
    elseif outtemp(1,7) == 2931 
    %elseif outtemp(1,7) == 2931 %updated from loss of 2791 
        outtemp(:,8) = 7;
        outtemp(:,9) = 0;
    elseif outtemp(1,7) == 2792
        outtemp(:,8) = 2;
        outtemp(:,9) = 0;
    elseif outtemp(1,7) == 2793
        outtemp(:,8) = 3;
        outtemp(:,9) = 1;
    end
    
    %dpm, TimeLost, Temp, Angle, Nall_min
    for j = 10:14
        A = 'outtemp(:,j) = ';
        B = [files{i,1} '.' out_names{1,j}];
        C = ';';
        D = [A B C];
        eval(D);
        clear A B C D;
    end
    
    %clickmax
    outtemp(:,15) = 4096;
    
    %calc JD
    for j = 1:size(outtemp,1)
        outtemp(j,16) = JulianDay(outtemp(j,1),outtemp(j,2),outtemp(j,3));
    end
    
    %keep blanks for dni, panel, time lost class
    outtemp(:,17:19) = NaN;
    
    %tidal covariates
    %%%IF START OF FILE IS NOT DURING DAYLIGHT SAVINGS, NEED TO SUBTRACT 4
    %%%HOURS FROM OUTTEMP(1,6)
    load('Z:\Projects\FORCE EEMP\Tidal covariates\combinedTidalData.mat');
    tidaldata(:,1) = datenum_round_off(tidaldata(:,1),'minute','round'); %make sure the datenum is rounded to the nearest minute.
    %tidaldata(:,1) = datenum_round_off(tidaldata(:,1),'minute','round'); %make sure the datenum is rounded to the nearest minute.
    index = find(tidaldata(:,1) == UTC(1,1) & tidaldata(:,4) == outtemp(1,8));
    outtemp(:,20:21) = tidaldata(index:size(outtemp,1)-1+index,2:3);
    
    %BinDPM
    outtemp(:,22) = 0;
    index = find(outtemp(:,10) > 0);
    outtemp(index,22) = 1;
    
    
    output(sum(rows(1:i,1))-rows(i,1) + 1: sum(rows(1:i,1)),:) = outtemp;
    clear outtemp;
end

%output(:,6) = datenum_round_off(output(:,6),'minute');   %roundoff the matdate to nearest minute

%% save csv file for analysis in R

%change date/time header names for output file
out_names{1,1} = 'year';
out_names{1,2} = 'month';
out_names{1,3} = 'day';
out_names{1,4} = 'hour';
out_names{1,5} = 'minute';

csvwrite_with_headers('BOF_data_ADT_Aug2019.csv',output,out_names);  %write the file to csv NOTE: MATDATE IS CORRUPTED IN THIS PROCESS

