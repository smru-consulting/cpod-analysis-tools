%takes the data matrix from cpodDetEnv2mat and calculates the dpm/day
%Jason Wood May 2, 2012

%determine which files are in the workspace
files = who;

%% calculate the dpm per day for each file and add to each file

for i = 1:size(files,1)
    %create vector of day
    A = 'daytemp = floor(';
    B = files{i,1};
    C = '.matdate);';
    D = [A B C];
    eval(D);
    clear A B C D;
    
    %create vector of dpm
    A = 'dpmtemp =';
    B = files{i,1};
    C = '.dpm;';
    D = [A B C];
    eval(D);
    clear A B C D;
    
    %calc dpm per day
    k = 1;  %start index counter
    dpm(1,1) = dpmtemp(1,1);  %start dpm at first entry
    day(1,1) = daytemp(1,1); %start day at first entry
    for j = 2:size(daytemp,1)
        if daytemp(j,1) == daytemp(j-1,1)
            dpm(k,1) = dpm(k,1) + dpmtemp(j,1); %sums each successive dpm for that day
        else
            k = k + 1;
            day(k,1) = daytemp(j,1);
            dpm(k,1) = dpmtemp(j,1);
        end
    end
    
    %save the dpm per day and day vectors to the original structures
    A = files{i,1};
    B = '.dpmDay = dpm;';
    C = [A B];
    eval(C);
    
    B = '.matDay = day;';
    C = [A B];
    eval(C);
    clear A B C dpm day daytemp dpmtemp;
end
