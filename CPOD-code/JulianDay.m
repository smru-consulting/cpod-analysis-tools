function jd = JulianDay(Year, Month, Day, Hour, Minute, Second)
% function jd = JulianDay(Year, Month, Day, Hour, Minute, Second)
% compulsory input arguments Year, Month, Day
% Optional input arguments Hour, Minute, Second
  iM = [1:12];
  DaysInMonth = eomday(Year, iM);
  jd = 0;
  if (Month > 1), jd = sum(DaysInMonth(1:Month-1)); end;
  jd = jd + Day;
  if nargin > 3
    jd = jd + Hour/24;
  end
  if nargin > 4
    jd = jd + Minute/24/60;
  end
  if nargin > 5
    jd = jd + Second/24/3600;
  end