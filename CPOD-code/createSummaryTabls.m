
% Create a table for detection positive minutes per day (useful for the
% interum report). This can be used after running the conCatCP3txt.m

close all; clear all; clc;

% Location of the project file
projectLoc = 'C:\Data\FORCE Data\2019 Aug - 2019 Dec\';
projectLoc ='C:\Data\FORCE Data\2020EEMP\10MINDetFiles\'

% Get the files within the project 
files = (dir([projectLoc '**\*.mat']));

% Load each deployment and create the table for survey days, detection
% positive 10 minute periods, detection periods
summaryTable = cell2table(cell(length(files),11),...
   'VariableNames',{'Loc','Mean', 'lowerCI',...
   'upperCI', 'N', 'NDays', 'StartDate', 'StopDate', 'propPos',...
   'n10Periods','nDetPos'})

% Confidence interval function
CIFcn = @(x,p)prctile(x,abs([0,100]-(100-p)/2));
p =[5 95]

% Create table for all workspaces
T = table();

% Summary table for each deployment and create the workspace for all
for ii=1:length(files)

    % Load the data
    [alldata]= load(fullfile(files(ii).folder, files(ii).name));
    data = alldata.T;
    
    data.DPMbinary = data.DPM>0;
    
    summaryTable.Loc(ii) = {data.LocName(1,:)};
    summaryTable.Pod(ii) = {data.pod(1)}
    summaryTable.Mean(ii) = {mean(data.DPMbinary)*100};
    summaryTable.N(ii) = {height(data)};
    summaryTable.StartDate(ii) = {datestr(min(data.ChunkEnd), 'yyyy/mm/dd')};
    summaryTable.StopDate(ii) ={datestr(max(data.ChunkEnd), 'yyyy/mm/dd')}
    
    % Calculate the confidence intervals
    aa = varfun(@mean,data,'InputVariables','DPMbinary',...
       'GroupingVariables','JulianDay');
    
    x = aa.mean_DPMbinary;
    
   
    CIs= CIFcn(x, p)*100
    summaryTable.lowerCI(ii) = {CIs(1)};
    summaryTable.upperCI(ii) = {CIs(2)};
    summaryTable.NDays(ii) = {length(unique(data.JulianDay))};
   
    
    % Total detection Positive periods
    summaryTable.propPos(ii) = {sum(data.DPMbinary)/height(data)};
    summaryTable.n10Periods(ii) = {height(data)};
    summaryTable.nDetPos(ii) = {sum(data.DPMbinary)};

    
   T = [T; data]; 
   
   figure(1)
   subplot(length(files), 1, ii) 
   scatter(data.Mdate, data.x_TimeLost)
   xlim([datenum('2020/06/10',  'yyyy/mm/dd') datenum('2020/10/05',  'yyyy/mm/dd')])
   datetick('x','yyyy-mmm')
   title(data.LocName(1,:)) 
    

end
% Matlab Date
T.matlabDate = round(datenum(T.ChunkEnd));


%Total number of CPOD days
Nmonitored = sum(cell2mat(summaryTable.NDays));

% Number of ten minute intervals
Nintervals = sum(cell2mat(summaryTable.N));

% Number of monitored days (CHECK THIS!)
NDays = max(cell2mat(summaryTable.NDays));

% Deployment Start and End times

% Total Porpoise Present days;
dailyPres = varfun(@max,T,'InputVariables','DPMbinary',...
       'GroupingVariables','matlabDate');
   
porPresentDays = sum(dailyPres.max_DPMbinary)/height(dailyPres)

% Days without porpoise presence
porNegDays = sum(-(dailyPres.max_DPMbinary-1))/height(dailyPres)



% Total Porpoise Present days;
dailyPres_grouped = varfun(@max,T,'InputVariables','DPMbinary',...
       'GroupingVariables',{'matlabDate', 'LocName'});

meanporPresentDays = varfun(@sum,dailyPres_grouped,'InputVariables','max_DPMbinary',...
       'GroupingVariables',{'LocName'});
   
   
meanporPresentDays.propPres = meanporPresentDays.sum_max_DPMbinary./...
    meanporPresentDays.GroupCount;
mean(meanporPresentDays.sum_max_DPMbinary)/mean(meanporPresentDays.GroupCount)






%Median and IQR of dp10m if present
% Remove days without detections

dailyTot_grouped = varfun(@sum,T,'InputVariables','DPM',...
       'GroupingVariables',{'matlabDate', 'LocName'});

% Remove dates without detections
dailyTot_grouped = dailyTot_grouped(dailyTot_grouped.sum_DPM>0,:);
   
medianDailyDPM = median(dailyTot_grouped.sum_DPM)

sortedDPM = sort(dailyTot_grouped.sum_DPM);
lowerQuartile = median(sortedDPM(1:length(sortedDPM)/2))
upperQuartile = median(sortedDPM(length(sortedDPM)/2:end))



% Number of days 'in region'

% Mean overall perct porp pres
mean([99.2 95.6 99 98.8 100 97.3 100 100 100 100 100 100 100 100 100])

%perct across cpod porp pres days
mean([83.2 82.9 87.5 92.5 76.4 73.8 92.4 95.2 100 96.9 88.3 88.3 98 91.04 93.4])

figure
% Histogram of time lost
[kk] =histogram(T.x_TimeLost, [0:5:100], 'Normalization', 'pdf',...
    'facecolor', 'y');
ylabel('Proportion of Data Collected')
xlabel('% Time Lost')
Bh = bar(bb,aa,'facecolor','y');

%% Exeuate summary numbers


% Monts and years covered- pull from turn on/deployment data
% dateStarts =datenum(cell2mat(vertcat(summaryTable.StartDate)), 'yyyy/mm/dd');
% datestr(min(dateStarts))
% datestr(max(dateStarts))


% Total C-POD days
sum(cell2mat(vertcat(summaryTable.NDays)))

% Median minutes of porpoise presence per day
medianDailyDPM = median(dailyTot_grouped.sum_DPM)

% site with highest proportion of DPM
[val, idx] = max(cell2mat(summaryTable.propPos))
summaryTable.Loc(idx)

%range of other sites
[val] = min(cell2mat(summaryTable.propPos(setxor(idx,1:height(summaryTable)))))
[val] = max(cell2mat(summaryTable.propPos(setxor(idx,1:height(summaryTable)))))

% Mean lost time due to sediment
median(T.x_TimeLost)


%% Methods
% Reorder the table to make transposing easier

summaryTable.reportOrder = [4 2 5 1 3]';
summaryTable = sortrows(summaryTable, 'reportOrder');

%% Overall summary of detection rates

totalPodDays= sum([958	391	689	252	225	332	262	146	39	357	520	480	542	367	453	506	636	426])
totCalandarDays =sum([258	137	208	84 45 73 57	41 13 91 104 ...
    99 111 85 117 103 152 max(cell2mat(summaryTable.NDays))])
% number of pod days
sum(cell2mat(vertcat(summaryTable.NDays)))

% ten minute periods
sum(cell2mat(vertcat(summaryTable.n10Periods)))

% Percent of calandar days with propoise detections
sum(cell2mat(vertcat(summaryTable.NDays)))

% Average detetion positive minutes per day
medianDailyDPM = median(dailyTot_grouped.sum_DPM)

% total percent of 10min periods with detections
sum(cell2mat(vertcat(summaryTable.nDetPos)))/sum(cell2mat(vertcat(summaryTable.n10Periods)))

%Across individual C-PODs, detection rates averaged 
mean(meanporPresentDays.sum_max_DPMbinary)/mean(meanporPresentDays.GroupCount)


% Median minutes of porpoise presence per day
medianDailyDPM = median(dailyTot_grouped.sum_DPM)
lowerQuartile
upperQuartile

%% Table 2

%%%% for the deployment %%%%
% number of days monitored
nDep = sum(cell2mat(vertcat(summaryTable.NDays)))

% number of pod days
sum(cell2mat(vertcat(summaryTable.NDays)))

% number of 10 minute periods
sum(cell2mat(vertcat(summaryTable.n10Periods)))


%%% total  %%%
totalPodDays= sum([958	391	689	252	225	332	262	146	39	357	520	480	542	367	453	506	636	426])
totCalandarDays =sum([258	137	208	84 45 73 57	41 13 91 104 ...
    99 111 85 117 103 152 max(cell2mat(summaryTable.NDays))])


totMonitoringPeriods10min =sum([136446	56795	99108	35775	32065	47403	37229	20756	5382	51009	74135	68094	77419	51722	64418	72090	90600	62392
])

%% Table 3

%%% for the deployment %%%%


% Total Porpoise Present days;
porPresentDays = sum(dailyPres.max_DPMbinary)/height(dailyPres)

% Overall percent days porp pos (porp pos calandar days)
mean(meanporPresentDays.sum_max_DPMbinary)/mean(meanporPresentDays.GroupCount)

% Days without porpoise presence and total days
porNegDays = sum(-(dailyPres.max_DPMbinary-1))/height(dailyPres)
height(dailyPres)


%%% For the overall rates
% overall percent porp pos- data missing just do average of previous
% total*number of deployments and new data
% Mean overall perct porp pres
mean([99.2 95.6 99 98.8 100 97.3 100 100 100 100 100 100 100 100 100])

%perct across cpod porp pres days
mean([83.2 82.9 87.5 92.5 76.4 73.8 92.4 95.2 100 96.9 88.3 88.3 98 91.04 93.4])

% Days without porpose and days monitored
sum([2 6 2 1 0 2 0 0 0 0 0 0 0 0 0])
sum([258 137 208 84 45 73 57 41 13 91 104 99 111 152 110])

% Average detetion positive minutes per day
medianDailyDPM = median(dailyTot_grouped.sum_DPM)
lowerQuartile
upperQuartile



% Values from previous reports Median IQR and percentiles det pos min
medianMinPorpDetMin  = [7 5 9 7 4 3 7 7 12 12 8 7 12 11 17];
lowerIQR =[2 1 3 3.75 1 0 3 4 7 6 2.75 2 6 5 10];
upperIQR =[17 13 16 14 10 7 14 12 19 21 20 16 20 19.5 24];

median(medianMinPorpDetMin)
median(lowerIQR)
median(upperIQR)

%% Location detection rates

% highest detection sites

% percentage of time lost and IQR
median(T.x_TimeLost)
prctile(T.x_TimeLost, .25)

%% Discussion 
medain(T.x_TimeLost)
totalPodDays
totCalandarDays
totMonitoringPeriods10min
mean(meanporPresentDays.sum_max_DPMbinary)/mean(meanporPresentDays.GroupCount)
medianDailyDPM